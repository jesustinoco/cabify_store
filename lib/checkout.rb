class Checkout
	def initialize(pricing_rules)
		@pricing_rules = pricing_rules
		@cart = {}
		pricing_rules.each {|product| @cart[product.code] = { "instance" => product, "amount" => 0}  }
	end

	def scan(product)
		if !@cart.has_key?(product)
			raise "The %s product should exist" % [product]
		end
		@cart[product]["amount"] += 1
	end

	def total()
		total = 0
		@cart.each do |code, product|
			amount = product["amount"]
			price = product["instance"].price
			if product["instance"].discount != nil
				total += product["instance"].discount.apply_discount(amount, price)
			else
				total += amount * price
			end
		end
		return total
	end
end