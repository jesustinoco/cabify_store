require_relative 'lib/checkout'
require_relative 'lib/product'
require_relative 'lib/discount'

bulkDiscount = BulkPurchasesDiscount.new(3, 19.0)
discount2for1 = TwoForOneDiscount.new()

pricing_rules = [
	Product.new("VOUCHER", "Cabify Voucher", 5.0, TwoForOneDiscount.new()), 
	Product.new("TSHIRT", "Cabify T-Shirt", 20.0, BulkPurchasesDiscount.new(3, 19.0)), 
	Product.new("MUG", "Cabify Coffe Mug", 7.50, nil) 
]

co = Checkout.new(pricing_rules)
co.scan("VOUCHER")
co.scan("TSHIRT")
co.scan("MUG")
puts co.total

co2 = Checkout.new(pricing_rules)
co2.scan("VOUCHER")
co2.scan("TSHIRT")
co2.scan("VOUCHER")
puts co2.total

co3 = Checkout.new(pricing_rules)
co3.scan("VOUCHER")
co3.scan("TSHIRT")
co3.scan("TSHIRT")
co3.scan("TSHIRT")
co3.scan("TSHIRT")
puts co3.total

co4 = Checkout.new(pricing_rules)
co4.scan("VOUCHER")
co4.scan("VOUCHER")
co4.scan("VOUCHER")
co4.scan("TSHIRT")
co4.scan("TSHIRT")
co4.scan("TSHIRT")
co4.scan("MUG")
puts co4.total
