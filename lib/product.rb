class Product
	attr_accessor :code, :name, :price, :discount
	
	def initialize(code, name, price, discount)
		@code = code
		@name = name
		@price = price
		@discount = discount
	end

end