class TwoForOneDiscount
	def initialize()
	end
	def apply_discount(amount, current_price)
		return (amount / 2 + amount % 2) * current_price
	end
end

class BulkPurchasesDiscount
	def initialize(purchases, new_price)
		@purchases = purchases
		@new_price = new_price
	end
	def apply_discount(amount, current_price)
		return amount >= @purchases ? amount * @new_price : amount * current_price 
	end
end