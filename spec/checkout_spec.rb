require 'checkout'
require 'discount'
require 'product'

RSpec.describe "Checkout" do
	
	bulkDiscount = BulkPurchasesDiscount.new(3, 19.0)
	discount2for1 = TwoForOneDiscount.new()

	pricing_rules = [
		Product.new("VOUCHER", "Cabify Voucher", 5.0, TwoForOneDiscount.new()), 
		Product.new("TSHIRT", "Cabify T-Shirt", 20.0, BulkPurchasesDiscount.new(3, 19.0)), 
		Product.new("MUG", "Cabify Coffe Mug", 7.50, nil) 
	]

	context "after executing the total" do
		it "whitout any discount should return 32.5" do
			checkout = Checkout.new(pricing_rules)
			checkout.scan("VOUCHER")
			checkout.scan("TSHIRT")
			checkout.scan("MUG")
			expect(32.5).to eq checkout.total()
		end
		it "with the 2for1 discount should return 25.0" do
			checkout = Checkout.new(pricing_rules)
			checkout.scan("VOUCHER")
			checkout.scan("TSHIRT")
			checkout.scan("VOUCHER")
			expect(25.0).to eq checkout.total()
		end
		it "with the bulk discount should return 81.0" do
			checkout = Checkout.new(pricing_rules)
			checkout.scan("VOUCHER")
			checkout.scan("TSHIRT")
			checkout.scan("TSHIRT")
			checkout.scan("TSHIRT")
			checkout.scan("TSHIRT")
			expect(81.0).to eq checkout.total()
		end
		it "with both bulk and 2for1 discounts should return 74.5" do
			checkout = Checkout.new(pricing_rules)
			checkout.scan("VOUCHER")
			checkout.scan("VOUCHER")
			checkout.scan("VOUCHER")
			checkout.scan("TSHIRT")
			checkout.scan("TSHIRT")
			checkout.scan("TSHIRT")
			checkout.scan("MUG")
			expect(74.5).to eq checkout.total()
		end
	end
end