require "discount"

RSpec.describe "Discount" do
	context "TwoForOneDiscount" do
		it "should not apply discount" do
			expect(5).to eq TwoForOneDiscount.new().apply_discount(1, 5)
		end

  		it "with 2 purchases should apply discount once" do
    		expect(5).to eq TwoForOneDiscount.new().apply_discount(2, 5)
  		end

  		it "with 3 purchases should apply discount once" do
    		expect(10).to eq TwoForOneDiscount.new().apply_discount(3, 5)
  		end

		it "with 4 purchases should apply discount twice" do
    		expect(10).to eq TwoForOneDiscount.new().apply_discount(4, 5)
  		end
	end
	context "BulkPurchasesDiscount" do
		it "with less than 3 purchases should not apply discount" do
			expect(10).to eq BulkPurchasesDiscount.new(3, 4).apply_discount(2, 5)
		end

  		it "with 3 purchases should apply discount" do
    		expect(12).to eq BulkPurchasesDiscount.new(3, 4).apply_discount(3, 5)
  		end

  		it "with 4 purchases should apply discount" do
    		expect(16).to eq BulkPurchasesDiscount.new(3, 4).apply_discount(4, 5)
  		end
	end
end